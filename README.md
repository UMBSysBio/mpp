# MeganPostProcessing (MPP)

R scripts for combining functional and taxonomic information derived from [Megan](https://uni-tuebingen.de/fakultaeten/mathematisch-naturwissenschaftliche-fakultaet/fachbereiche/informatik/lehrstuehle/algorithms-in-bioinformatics/software/megan6/) as used in [Becker et al. (in prep.)](https://).
First, use `AnnotationCombine.R` to combine functional and taxonomic information for each sample, and then use `TableCombine.R` to combine resulting info across multiple samples.

To export taxonomic data from Megan, uncollapse the taxonomic tree, select the desired subtrees (including all sub-nodes), and export as CVS, selecting "read-id-taxonomic-path-including-percentage" (export what=CSV format=readName\_to\_taxonPathPercent separator=tab counts=summarized).
To export functional data from Megan, uncollapse the functional tree and select all leaf nodes only, and export as CVS, selecting "read-id-functional-path" (export what=CSV format=readName\_to\_eggnogPath separator=tab counts=summarized). Make sure that for both files, the number of lines coincides with values reported in Megan's tree visualizations. The script assumes that for each read, there is one unique annotation for taxonomy and/or function.

## Authors

* **Daniela Becker** (TableCombine.R), UFZ - Helmholtz Centre for Environmental Research, Leipzig, Germany
* **Florian Centler** (AnnotationCombine.R), UFZ - Helmholtz Centre for Environmental Research, Leipzig, Germany

Contact: daniela.taraba@ufz.de

## License

These scripts are licensed under the GNU General Public License v3.0 - see the [LICENSE](LICENSE) file for details
